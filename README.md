# find-file-format

Program helps to determine the format of a file, even if the file without extension.

## Usage

program-name FILE

## License

Copyright © 2015 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
