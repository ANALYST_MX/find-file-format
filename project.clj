(defproject find-file-format "0.1.0-SNAPSHOT"
  :description "Program helps to determine the format of a file, even if the file without extension."
  :url "https://bitbucket.org/ANALYST_MX/find-file-format/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/tools.cli "0.3.1"]
                 [byte-streams "0.2.0-alpha8"]
                 [clojurewerkz/propertied "1.2.0"]]
  :main find-file-format.core)
