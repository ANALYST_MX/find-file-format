(ns find-file-format.core
  (:use [clojure.java.io :only [resource as-file]])
  (:require [clojure.tools.cli :refer [parse-opts]]
            [clojure.string :as string]
            [byte-streams :refer [to-byte-array]]
            [clojurewerkz.propertied.properties :as p])
  (:gen-class))

(defonce types (p/properties->map (p/load-from (resource "types.properties"))))

(def min-length (/ (apply min (map #(count %) (keys types))) 2))

(def max-length (/ (apply max (map #(count %) (keys types))) 2))
  
(defn read-partial [file length]
  (let [hex-format #(->> "%02X" (repeat %) (interpose "") (apply str))]
    (apply format
           (str
            (hex-format length))
           (byte-streams/to-byte-array file))))

(defn get-type [magic-number]
  (when (>= (/ (count magic-number) 2) min-length)
    (if-let [type (get types magic-number)]
      type
      (recur (apply str (drop-last 2 magic-number))))))

(defn ->file [path]
  (as-file path))

(defn t [file]
  (if (and (.exists file) (>= (.length file) max-length))
    (if-let [type (get-type (read-partial file max-length))]
      type
      "Unknown Format")
    "Error reading from file"))

(def cli-options
  [["-h" "--help"]])

(defn usage [options-summary]
  (->> ["Program helps to determine the format of a file,"
        "even if the file without extension."
        ""
        "Usage: program-name FILE"
        ""
        "Options:"
        options-summary
        ""
        "Please refer to the manual page for more information."]
       (string/join \newline)))

(defn error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (string/join \newline errors)))

(defn exit [status msg]
  (println msg)
  (System/exit status))

(defn -main [& args]
  (let [{:keys [options arguments errors summary]} (parse-opts args cli-options)]
    (cond
      (:help options) (exit 0 (usage summary))
      (not= (count arguments) 1) (exit 1 (usage summary))
      errors (exit 1 (error-msg errors)))
      (println (t (->file (first arguments))))))
